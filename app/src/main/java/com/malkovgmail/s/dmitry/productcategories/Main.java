//TODO: План действий
//TODO: + Реализовать onBackPressed(), нужен запрос в БД для возврата верного курсора
//TODO: - ArrayList<Long> parentId сохраняет id без без дочерних элементов, ограничить сохранение
//TODO: - Кнопка обновления

package com.malkovgmail.s.dmitry.productcategories;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main extends AppCompatActivity {

    public static final String TAG = "myLog";
    DB db;
    ListView lvData;
    Main actv;
    //List<Long> parentId;
    ArrayList<Long> parentId = new ArrayList<Long>(Arrays.asList(0L));
    SimpleCursorAdapter scAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        actv = this;

        db = new DB(this);
        db.open();


        lvData = (ListView) findViewById(R.id.lvData);
        lvData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG, "itemClick: position = " + position + ", id = " + id);
                parentId.add(id);
                new ConnectParserTask(actv, db).execute(String.valueOf(id));
            }
        });

        // формируем столбцы сопоставления
        String[] from = new String[] { DB.COL_TITLE};
        int[] to = new int[] { R.id.tvText };

        // создаем адаптер и настраиваем список
        scAdapter = new SimpleCursorAdapter(this, R.layout.item, null, from, to,0);
        lvData.setAdapter(scAdapter);
        Log.i(TAG, "AsyncTask");
        new ConnectParserTask(actv, db).execute(String.valueOf(parentId.get(0)));
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // TODO: ошибка - нужен дополнительный запрос по id
        Log.i(TAG, "--onBackPressed " + parentId.get(parentId.size() - 1));
        if(parentId.size() != 1){
            int pID = parentId.size() - 1;
            //new ConnectParserTask(actv, db).execute(String.valueOf(parentId.get(parentId.size() - 1)));
            Log.i(TAG, "parentID = " + parentId.get(pID));
            Cursor c = db.getParentCategory(String.valueOf(parentId.get(pID)));
            scAdapter.swapCursor(c);
            parentId.remove(pID);
        }else{
            super.onBackPressed();
        }

    }
}
