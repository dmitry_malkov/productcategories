package com.malkovgmail.s.dmitry.productcategories;

import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.support.v4.widget.SimpleCursorAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

class ConnectParserTask extends AsyncTask<String, String, Cursor> {
    Main activity = null;
    DB db;
    Cursor c;

    public static final String urlStr = "https://money.yandex.ru/api/categories-list";

    public ConnectParserTask(Main main ,DB db) {
        this.activity = main;
        this.db = db;
    }

    @Override
    protected Cursor doInBackground(String... args) {
        Log.i(activity.TAG, "--doInBackground");

        String selection = DB.COL_ID_PARENT + " = ?";
        String[] selectionArgs = new String[] { args[0] };
        String n = "0";
        c = db.getCategory(selection, selectionArgs);
        if (c.moveToNext()) {
            Log.i(activity.TAG,"--data OK");

        } else if(args[0].equals(n) || args.length == 2) {
            Log.i(activity.TAG,"--data NO");
            parserJson(connectUrl(), 0);
            c = db.getCategory(selection, selectionArgs);
        }else{
            this.cancel(false);
        }
        return c;
    }

    @Override
    protected void onPostExecute(Cursor c){
        super.onPostExecute(c);
        Log.i(activity.TAG, "--onPostExecute");
        activity.scAdapter.swapCursor(c);
    }

    protected String connectUrl() {
        Log.i(activity.TAG, "--connectUrl");

        HttpURLConnection urlConnection = null;
        BufferedReader r = null;
        String jsonStr = null;

        try {
            URL url = new URL(urlStr);
            urlConnection = (HttpURLConnection) url.openConnection();

            StringBuffer jsonBuf = new StringBuffer();
            r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = r.readLine()) != null) {
                jsonBuf.append(line);
            }

            jsonStr = jsonBuf.toString();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                r.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            urlConnection.disconnect();
        }

        return jsonStr;
    }

    /**
     * Сохраняем данные из json в SQLite
     *
     * @param jsonStr
     * @param parentId
     */
    public void parserJson (String jsonStr, int parentId  ) {
        Log.i(activity.TAG,"--parserJson");

        if (!jsonStr.isEmpty()) {

            JSONArray jsonArr = null;

            try {
                jsonArr = new JSONArray(jsonStr);
                for (int i = 0; i < jsonArr.length(); i++) {
                    String title = "";
                    String subs = "";
                    int id_item = 0;
                    int id = 0;

                    JSONObject category = jsonArr.getJSONObject(i);

                    Iterator<String> iter = category.keys();

                    while (iter.hasNext()) {
                        String t = iter.next();
                        switch (t) {
                            case "id":
                                id_item = category.getInt(t);
                                break;
                            case "title":
                                title = category.getString(t);
                                break;
                            case "subs":
                                subs = category.getString(t);
                                break;
                        }
                    }

                    id = (int) db.addCategory(title, id_item, parentId);
                    Log.i(Main.TAG, "insert id - " + id);
                    if (!subs.isEmpty() && id != -1) {
                        this.parserJson(subs, id);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}