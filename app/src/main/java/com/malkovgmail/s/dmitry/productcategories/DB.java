package com.malkovgmail.s.dmitry.productcategories;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DB {

    private static final String TAG = "myLogDB";

    private static final String DB_NAME = "myDB";
    private static final int DB_VERSION = 1;
    private static final String DB_TABLE_CATEGORIES = "categories";

    public static final String COL_ID = "_id";
    public static final String COL_TITLE = "title";
    public static final String COL_ID_ITEM = "id_item";
    public static final String COL_ID_PARENT = "id_parent";

    private static final String DB_CREATE_TABLE =
            "create table " + DB_TABLE_CATEGORIES + "(" +
            COL_ID + " integer primary key autoincrement, " +
            COL_TITLE + " text, " +
            COL_ID_PARENT + " integer, " +
            COL_ID_ITEM + " integer);";

    private final Context mCtx;

    private DBHelper mDBHelper;
    private SQLiteDatabase mDB;

    public DB(Context mCtx) {
        this.mCtx = mCtx;
    }

    public void open(){
        Log.i(TAG, "--OPEN");
        mDBHelper = new DBHelper(mCtx,DB_NAME,null,DB_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }

    /**
     *
     * @param title
     * @param id_item
     * @param id_parent
     * @return id
     */
    public long addCategory(String title, int id_item,int id_parent){
        ContentValues cv = new ContentValues();
        cv.put(COL_TITLE, title);
        cv.put(COL_ID_ITEM,id_item);
        cv.put(COL_ID_PARENT, id_parent);
        return  mDB.insert(DB_TABLE_CATEGORIES, null,cv);
    }

    public void deleteCategory(){
        mDB.delete(DB_TABLE_CATEGORIES, null, null);
    }

    public Cursor getCategory(String selection,String[] selectionArgs){
        return mDB.query(DB_TABLE_CATEGORIES,null,selection,selectionArgs,null,null,null);
    }

    public Cursor getParentCategory(String idP){
        String selection = DB.COL_ID + " = ?";
        String[] selectionArgs = new String[] { idP };
        //String[] columns = new String[] { COL_ID_PARENT };
        Cursor c = mDB.query(DB_TABLE_CATEGORIES,null,selection,selectionArgs,null,null,null);
        Cursor c2 = null;
        if(c.moveToNext()){
            int idColParent = c.getColumnIndex(COL_ID_PARENT);
            int id = c.getInt(idColParent);
            Log.i(TAG,"idP = " + idP + " -ID = " + idColParent + " value = " + c.getInt(idColParent) + " title = " + c.getString(1));

            selection = DB.COL_ID_PARENT + " = ?";
            selectionArgs = new String[] { String.valueOf(id) };
            c2 = mDB.query(DB_TABLE_CATEGORIES,null,selection,selectionArgs,null,null,null);

        }
        c.close();
        return c2;
    }

    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.i(TAG, "--onCreate");
            db.execSQL(DB_CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
